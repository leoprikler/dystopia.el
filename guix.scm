(use-modules (guix gexp)
             (guix packages)
             (guix git-download)
             (guix utils)
             (guix build-system copy)
             (gnu packages bash)
             (gnu packages game-development)
             ((guix licenses) #:prefix license:))

(define %source-dir (dirname (current-filename)))

(package
 (name "dystopia")
 (version "0.2")
 (source
  (local-file %source-dir #:recursive? #t
              #:select? (if (file-exists? (string-append %source-dir "/.git"))
                            (git-predicate %source-dir)
                            (const #t))))
 (build-system copy-build-system)
 (arguments
  `(#:install-plan
    '(("README.org" ,(string-append "share/doc/dystopia-" version "/"))
      ("." "share/tsukundere/dystopia.el/assets"
       #:include-regexp (".*\\.(png\|jpg\|ogg\|wav\|ttf)" ))
      ("." "share/tsukundere/dystopia.el/source"
       #:include-regexp (".*\\.el")))
    #:phases
    (modify-phases %standard-phases
      (add-after 'install 'install-launcher
        (lambda* (#:key inputs outputs #:allow-other-keys)
          (let* ((bash (assoc-ref inputs "bash"))
                 (tsukundere (assoc-ref inputs "tsukundere"))
                 (out (assoc-ref outputs "out"))
                 (dystopia.el (string-append out "/share/tsukundere"
                                             "/dystopia.el")))
            (mkdir-p (string-append out "/bin"))
            (call-with-output-file (string-append out "/bin/dystopia")
              (lambda (launcher)
                (format launcher "#!~a/bin/sh
exec ~a/bin/tsukundere run --asset-dir=~s --load-path=~s --module=~s"
                        bash tsukundere
                        (string-append dystopia.el "/assets")
                        (string-append dystopia.el "/source")
                        "dystopia")))
            (chmod (string-append out "/bin/dystopia") #o555)))))))
 (inputs `(("bash" ,bash)
           ("tsukundere" ,tsukundere)))
 (home-page "https://gitlab.com/lilyp/dystopia.el")
 (synopsis "Not to be confused with real life")
 (description "Yuu thought, Yuu were attending a normal high school.  But
actually this school indoctrinates children, so that they grow up into obedient
wage slaves.  Can Yuu change this system or will Yuu end up as another
bootlicker?")
 (license
  (list license:gpl3+  ; script/package as a whole
        ;; assets
        license:cc0
        license:cc-by3.0
        license:cc-by-sa3.0
        license:cc-by4.0
        license:expat
        (license:non-copyleft "file://COPYING.mugen")
        (license:non-copyleft "file://COPYING.wifom")
        ;; fonts
        (license:x11-style "https://dejavu-fonts.github.io/"))))
